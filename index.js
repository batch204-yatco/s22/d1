// console.log("Hello 204");

/*
	Manipulating arrays with array methods
		- Javascript has built-in functions for arrays
		- Allows us to manipulate and access array items
		- Arrays can be mutated or iterated
*/

// Mutator Methods
/*
	- Functions that mutate or change an array after they are created
	- These methods manipulate original array by performing various tasks such as adding and removing/deleting elements
*/

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit", "Lemon"];
/*
	push()
		Adds element in the end of an array AND returns array's length

	syntax:
		arrayName.push();

*/
console.log("Current array:");
console.log(fruits);
let fruitLength = fruits.push("Mango");
console.log(fruitLength);//6
console.log("Mutated array from push method");
console.log(fruits);

// Adding multiple elements to an array
fruits.push("Avocado", "Guava");
console.log(fruits);

/*
	pop()
		Removes last element in an array AND returns removed element

	syntax:
		arrayName.pop();

*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method");
console.log(fruits);

/*
	unshift()
		Add one or more elements at beginning of array

	Syntax:
		arrayName.unshift("elementA");
		arrayName.unshift("elementA", elementB);
*/

fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method");
console.log(fruits);

/*
	shift()
		Removes element at beginning of an array AND returns removed element

	Syntax:
		arrayName.shift()
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method");
console.log(fruits);

/*
	splice()
		simultaneously removes an element from specified index number and adds an element

	Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated array from splice method");
console.log(fruits);

fruits.splice(1,2);//can delete only
console.log(fruits);

fruits.splice(2, 0, "Grapes");//can add only
console.log(fruits);

// fruits.splice(3);//removes elements after number
// console.log(fruits);

/*
	sort()
		Rearranges array elements in alphanumeric order

	Syntax:
		arrayName.sort()
*/

fruits.sort();
console.log("Mutated array from sort method");
console.log(fruits);

/*
	reverse()
		Reverses order of array elements

	Syntax:
		arrayName.reverse()
*/

// fruits.reverse();
// console.log("Mutated array from reverse method");
// console.log(fruits);

fruits.sort().reverse();
console.log(fruits);

//Non-Mutator Methods
/*
	- Non-Mutator methods are functions that do not modify or change array after they are created

	- These methods do not manipulate original array in performing various tasks
*/

let countries = ["US", "PH", "CA", "TH", "HK", "PH", "FR", "DE"];

/*
	indexOf()
		- Returns index number of first matching element found in array
		- If no match, result will be -1
		- Search process will be done from first element proceeding to last element

	Syntax:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(searchValue, fromIndex);
*/

let firstIndex = countries.indexOf("PH");//1
console.log("Result of indexOf method: "+firstIndex);

let invalidCountry = countries.indexOf("HW");
console.log("Result of indexOf method: "+invalidCountry);

/*
	lastIndexOf()
		- Returns index number of last matching element found in array
		- Search process will be undone from last element proceeding to first element

	Syntax:
		arrayName.lastIndexOf(searchValue);
		arrayName.lastIndexOf(searchValue, fromIndex);
*/

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method: "+lastIndex);//5

console.log(countries);

/*
	slice()
		- Portions/slices elements from array AND returns new array
		- Not like splice b/c this is non-mutator

	Syntax:
		arrayName.slice(startingIndex)
		arrayName.slice(startingIndex, endingIndex)
*/

// Slicing off elements from specified index to last element 
let slicedArrayA = countries.slice(2);
console.log("Result from slice method");
console.log(slicedArrayA);
console.log(countries);

let slicedArrayB = countries.slice(2, 4);//last index is not included in the return
console.log("Result from slice method");
console.log(slicedArrayB);

/*
	toString()
		Returns an array as a string by commas

	Syntax:
		arrayName.toString()
*/

let stringArray = countries.toString();
console.log("Result from toString method: ");
console.log(stringArray);

/*
	concat()
		Combines two arrays and returns combined result

	Syntax:
		arrayA.concat(arrayB)
		arrayA.concat(elementA);
*/

let taskArrayA = ["drink html", "eat javascript"];
let taskArrayB = ["inhale css", "breathe sass"];
let taskArrayC = ["get git", "be node"];

let tasks = taskArrayA.concat(taskArrayB);
console.log(tasks);

let alltasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(alltasks);

let combinedTasks = taskArrayA.concat("smell express", "throw react");
console.log(combinedTasks);

// Can we combine multiple arrays with elements?
let multipleTasks = taskArrayA.concat(taskArrayB, taskArrayC, "smell express", "throw react");
console.log(multipleTasks);

/*
	join()
		- Returns array as string separated by specified separator string

	Syntax:
		arrayName.join("separatorString");
*/

let users = ["Jack", "Raf", "Daphne", "Lexus"]; 
console.log(users.join());
console.log(users.join(" "));
console.log(users.join(" - "));

// Iteration Methods
/*
	- Loops designed to perform repetitive tasks on array
	- Loops over all items in array
*/

/*
	forEach()
		- Similar to for loop that iterates on each array element
		- For each item in array, the anonymous function (function w/o name) passed in forEach() method will be run
		- no need to set index conditions like for method

	Syntax:
		arrayName.forEach(function(indivElement){
			statement;
		});
*/
console.log(multipleTasks);
// anonymous function "parameter" represents each element of array to be iterated
multipleTasks.forEach(function(task) {
	console.log(task);
});

// for(let i = 0; i < multipleTasks.length; i++){
// 	console.log(multipleTasks[i]);
// };

// Using forEach with conditional statements 
let filteredTasks = [];

multipleTasks.forEach(function(task){

	console.log(task);
	
	if (task.length>10){
	
		filteredTasks.push(task)

	}

})

console.log("Result of filtered tasks: ");
console.log(filteredTasks);

/*
	map()
		- Iterates on each element AND returns new array with different values depending on result of the function's operation
		- Unlike forEach method, map method requires use of a "return" statement in order to create another array with the performed operation

	Syntax:
		let/const resultArray = arrayName.map(function(indivElement) {})
*/

let numbers = [1,2,3,4,5];
let numberMap = numbers.map(function(number){
	return number * number;
});

console.log("Original Array:");
console.log(numbers);
console.log("Result of map method:");
console.log(numberMap);

//map() vs forEach

let numberForEach = numbers.forEach(function(number){
	return number * number;
});

console.log(numberForEach); //undefined

/*
	every()
		- Checks if all elements in array meet given condition
		- Returns true value if all elements meet given condition and false if otherwise

	Syntax:
		let/const array = arrayName.every(function(indivElement){
			return expression/condition
		})
*/

let allValid = numbers.every(function(number){
		return (number < 3);
})

console.log("Result of every method");
console.log(allValid);

/*
	some()
		- Checks if at least one element in arrays meets given condition 
		- Return true value if at least one meets given condition, false if otherwise

	Syntax:
		let/const result = arrayName.some(function(indivElement){
			return expression/condition
		})
*/

let someValid = numbers.some(function(number){
	return (number < 2);
});
console.log("Result of some method:");
console.log(someValid);

if(someValid){
	console.log("Some numbers in array is are less than 2");
}

/*
	filter()
		- Return new array that contains elements which meet given condition
		- Return empty array if no elements found

	Syntax:
		let/const resultArray = arrayName.filter(function(indivElement){
			return expression/condition
		})
*/

let filterValid = numbers.filter(function(number){
	return (number < 3);
});
console.log("Result of filter method");
console.log(filterValid); //[1, 2]

// No elements found
let nothingFound = numbers.filter(function(number){
	return (number > 6);
});
console.log("This is the result of filter method: ");
console.log(nothingFound);

// Filtering using forEach
let filteredNumbers = [];
numbers.forEach(function(number){
	if(number < 3){
		filteredNumbers.push(number);
	}
});
console.log(filteredNumbers);

/*
	includes()
		- includes() method checks if argument passed can be found in array
		- returns boolean which can be saved in variable
		- returns false if it is not

	syntax:
		arrayName.includes()
*/
let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound = products.includes("Mouse");
console.log(productFound);

let productNotFound = products.includes("Headset");
console.log(productNotFound);

// Method Chaining

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes("a");
});
console.log(filteredProducts);